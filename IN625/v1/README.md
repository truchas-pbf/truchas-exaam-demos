truchas-in625-prop-v1.odp -- Summary slides documenting the Truchas
material properties for Inconel 625 (libreoffice format)

truchas-in625-prop-v2.pdf -- PDF of slides

.dat files are the raw data for individual properties extracted from
the master IN625.xlsx file

.agr are grace files (http://plasma-gate.weizmann.ac.il/Grace)

.png are plots exported from grace
