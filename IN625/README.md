Material Properties for Inconel 625
-----------------------------------

IN625.xlsx -- Temperature dependent property data provided by Naren Raghavan.
They were produced using the JMatPro software; see the file JMatPro.pdf for
a description of the software.

v1 -- Processed raw data (first version) for Truchas input: fits to analytic
function, plots, etc.
