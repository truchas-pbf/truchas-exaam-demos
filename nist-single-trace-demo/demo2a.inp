EXAMPLE SINGLE LASER SCAN ON BARE INCONEL 625 PLATE
---------------------------------------------------

NIST single laser scan on bare Inconel 625 plate studies.
* scans performed on EOS M270
* 1" x 1" x 1/8" thick plate
* 4mm long scans with variable laser power / scan speed
* steady state reached well within initial 1mm of scan
* estimate cylindrical Gaussian laser profile with sigma=35um (FWHM=82um) 
* nominal process parameters 195 W and 800 mm/s
* Cases (power/speed): 1) 49/200, 2) 122/200, 3) 122/500, 4) 122/800,
  5) 195/200, 6) 195/500, 7) 195/800
* Cases 2, 5, 6 (esp 5) are in the keyhole process space.

Here the setup is a 3x3x3mm plate, 1mm laser scan.

Model is posed in mm, g, ms, kelvin units.
 
Conversion factors from MKS units:
  1 Joule [kg m^2 / s^2] = 1e3 g mm^2 / ms^2
  1 Watt [J/s] = 1 [g mm^2 / ms^3]
  1 W/m^2 = 1e-6 [g mm^2 / s^3 mm^2]
  1 N [kg m / s^2] = 1 g mm / ms^2
  1 Pa [N/m^2] = 1e-6 (g-mm/ms^2)/mm^2
  1 Pa-s = 1 kg / m s = 1e-3 g/mm-ms

&MESH
  mesh_file = 'demo2-half-5um.gen'
/

&OUTPUTS
  output_t = 0.0, 1.25
  output_dt = 0.05
  !Use these to move the build plate in Paraview
  !and keep the laser position fixed
  !move_block_ids = 1
  !move_toolpath_name = 'plate'
/

Nominal process parameters for Inconel 625 on EOS 370 at NIST (Lyle Levine)
* 195 W, 800 mm/s, Gaussian profile FWHM = 82um

&DED_HEAD
  toolpath = 'laser'
  laser_type = 'gaussian'
  laser_time_const = 0.1 ! [ms]
  laser_absorp = 0.4
  laser_power = 195.0 ! [W]
  laser_sigma = 0.035 ! [mm]
/

! Laser axis tracks the toolpath (mm, ms units). Top of plate is z=0. 

&TOOLPATH
  name = 'laser'
  start_coord = -0.5, 0.0, 0.0
  command_string = '[["setflag",0],["moverel",[1,0,0],800e-3]]'
  !write_plotfile = .true.
  !plotfile_dt = 0.02
/

! This toolpath tracks the motion of the build plate relative to the laser
! (essentially the negative of the 'laser' toolpath).  Its only use is by
! the outputs namelist to record a translation of the build plate element
! block in the output file for later use in visualization.

&TOOLPATH
  name = 'plate'
  start_coord = 0.5, 0.0, 0
  command_string = '[["moverel",[-1,0,0],800e-3]]'
  write_plotfile = .true.
  plotfile_dt = 0.02
/
  
&PHYSICS
  fluid_flow = .false.
!  fluid_flow = .true.
!  inviscid = .false.
!  surface_tension = .true.
  body_force = 9.81e-3 ! mm/ms^2
  heat_transport = .true.
/

&PHYSICAL_CONSTANTS
  stefan_boltzmann = 5.67e-14   ! g-mm-ms units
  !absolute_zero = -273.15       ! temperature in celsius
/

&EVAPORATION
  face_set_ids = 1
  prefactor = 3.38e10
  temp_exponent = -0.5
  activation_energy = 3.72e5 
/

&SURFACE_TENSION
  csf_boundary = .true.
  bndry_face_set_ids = 1
  dsig_dt = -0.31e-6  ! [(g-mm/ms^2)/(mm-K)]
  sigma_constant = 1.0 ! not used, but input wants to see it set
/

&DIFFUSION_SOLVER
  abs_temp_tol = 0.0
  rel_temp_tol = 1.0e-2
  abs_enthalpy_tol = 0.0
  rel_enthalpy_tol = 1.0e-2
  nlk_tol = 0.02
  max_nlk_itr = 5
  nlk_preconditioner = 'hypre_amg'
  pc_amg_cycles = 2
  verbose_stepping = .true.
/

&NUMERICS
  dt_init = 1.0e-6
  dt_min = 1.0e-8
  !dt_grow = 1.05 ! when flow is enabled
  dt_grow = 5.0   ! when only heat transfer is enabled
  dt_max = 1.0e3
  courant_number = 0.3
  volume_track_subcycles = 4
  discrete_ops_type = 'ortho'
  projection_linear_solution = 'projection'
  viscous_implicitness = 0.5
  viscous_linear_solution = 'viscous'
/

&LINEAR_SOLVER
  name = 'projection'
  method = 'fgmres'
  preconditioning_method = 'ssor'
  convergence_criterion = 1.0e-10
  relaxation_parameter = 1.4
  preconditioning_steps = 2
  stopping_criterion = '||r||'
  maximum_iterations = 2000
/

&LINEAR_SOLVER
  name = 'viscous'
  method = 'fgmres'
  preconditioning_method = 'diagonal'
  stopping_criterion = '||r||/||b||'
  convergence_criterion = 1.0e-10
  relaxation_parameter = 1.0
  preconditioning_steps = 5
  maximum_iterations = 500
/

##### BOUNDARY CONDITIONS ######################################################

&DS_BOUNDARY_CONDITION
  name = 'top'
  variable = 'temperature'
  condition = 'flux'
  face_set_ids = 1
  data_function = 'laser'
/

&FUNCTION
  name = 'laser'
  type = 'ded head laser'
/

&DS_BOUNDARY_CONDITION
  name = 'top-rad'
  variable = 'temperature'
  condition = 'radiation'
  face_set_ids = 1
  data_constant = 0.6, 298.0
/

&DS_BOUNDARY_CONDITION
  name = 'bottom'
  variable = 'temperature'
  condition = 'flux'
  face_set_ids = 2
  data_constant = 0.0
/

&DS_BOUNDARY_CONDITION
  name = 'sides'
  variable = 'temperature'
  condition = 'flux'
  face_set_ids = 3
  data_constant = 0.0
/

&DS_BOUNDARY_CONDITION
  name = 'symmetry'
  variable = 'temperature'
  condition = 'flux'
  face_set_ids = 4
  data_constant = 0.0
/

##### ASSIGN MATERIALS AND INITIAL CONDITIONS ##################################

&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 1
  material_number = 1
  temperature = 298.0
/

##### MATERIALS ################################################################

&MATERIAL
  material_name = 'IN625-sol'
  material_number = 1
  immobile= .true.
  priority = 1
  density = 1.0 ! not void
  material_feature = 'background'
/

&PHASE 
 name = 'IN625-sol' 
 property_name(1) = 'density',       property_constant(1) = 7.57e-3 ! g/mm^3
 property_name(2) = 'specific heat', property_function(2) = 'IN625-sol-Cp'
 property_name(3) = 'conductivity',  property_function(3) = 'IN625-sol-k'
/

&FUNCTION ! (g-mm^2/ms^2)/g-K
  name = 'IN625-sol-Cp'
  type = 'polynomial'
  poly_coefficients = 428.38, 0.23638
  poly_exponents(1,:) = 0, 1
  poly_refvars(1) = 273.0
/

&FUNCTION ! (g-mm^2/ms^3)/mm-K
  name = 'IN625-sol-k'
  type = 'polynomial'
  poly_coefficients = 12.3e-3, 1.472e-5
  poly_exponents(1,:) = 0, 1
  poly_refvars(1) = 273.0
/

&MATERIAL
  material_name = 'IN625-liq'
  material_number = 2
  immobile= .false.
  priority = 2
  density = 1.0 ! not void
/

&PHASE
  name = 'IN625-liq'
  property_name(1) = 'density',       property_constant(1) = 7.57e-3   ! g/mm^3
  property_name(2) = 'specific heat', property_constant(2) = 750.65    ! (g-mm^2/ms^2)/g-K
  property_name(3) = 'conductivity',  property_function(3) = 'IN625-liq-k'
  property_name(4) = 'viscosity',     property_function(4) = 'IN625-visc'
  property_name(5) = 'density deviation', property_function(5) = 'IN625-liq-drho'
/

&FUNCTION ! (g-mm^2/ms^3)/mm-K
  name = 'IN625-liq-k'
  type = 'polynomial'
  poly_coefficients = 8.92e-3, 1.474e-5
  poly_exponents(1,:) = 0, 1
  poly_refvars(1) = 273.0
/

&FUNCTION ! [g/mm-ms]
  name = 'IN625-visc'
  type = 'polynomial'
  poly_coefficients = 4.0493e-6, -1.9397e-2, 34.735
  poly_exponents(1,:) = 0, -1, -2
  poly_refvars(1) = 273.0
/

&FUNCTION
  name = 'IN625-liq-drho'
  type = 'polynomial'
  poly_coefficients = 0.13567, -9.0197e-5, -7.9917e-9
  poly_exponents(1,:) = 0, 1, 2
  poly_refvars(1) = 273.0
/

&MATERIAL_SYSTEM
  name = 'IN625'
  phases = 'IN625-sol', 'IN625-liq'
  transition_temps_low  = 1531  ! [K]
  transition_temps_high = 1616  ! [K]
  smoothing_radius = 5 ! [K]
  latent_heat = 2.1754e5 ! [g-mm^2/ms^2/g]
/
