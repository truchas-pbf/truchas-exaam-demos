This is an initial single-laser-trace-on-plate simulation using NIST
process parameters for laser and laser speed.  It still uses 304 SS
properties (except surface tension), and not Inconel 625 as used in
the NIST experiments.

The .h5 output file is omitted due to its size.

The .pvsm is the viz configuration for Paraview 5.4, and .png
files are plot images from paraview.  See the powerpoint file
for a summary.
