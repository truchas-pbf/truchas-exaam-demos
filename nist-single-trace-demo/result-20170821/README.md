This is an initial single-laser-trace-on-plate simulation using NIST
process parameters for laser and laser speed.  It uses version 1 of
the Inconel 625 material properties.

The .h5 output file is omitted due to its size.

The startup.inp and example.inp files are renamed from demo2b-startup.inp
and demo2b.inp.
