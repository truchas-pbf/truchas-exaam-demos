Single laser scan on bare Inconel 625 plate inputs
--------------------------------------------------

`demo2a.inp` -- heat transfer only

demo2b -- heat transfer and fluid flow run in two stages:
Run `demo2b-startup.inp` (heat transfer only) to 0.25 ms to allow melt
pool to form, generate restart file, and then continue to final time with
`demo2b.inp` (heat transfer and fluid flow)
