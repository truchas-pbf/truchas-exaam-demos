Truchas ExaAM Demo Problems Repository
--------------------------------------

* nist-single-trace-exp-doc. Documents describing single laser trace on bare
  Inconel 625 plate experiments done by NIST.

* IN625. Property data for Inconel 625.

* nist-single-trace-demo.  Truchas input files for simulating the NIST experiments.

* single-track-validiation.  Truchas validation study for 2018 Q3 milestone
  using data from NIST's AM Bench 2018 Challenge.
