Truchas AM-Bench 2018 Single Track Validation
---------------------------------------------

Validation of the Truchas AM model using the AM Bench 2018 single track
challenge problem data. This is being done for a 2018Q3 ECP/ExaAM milestone.
This will use the "classic" Truchas, as the AMReX-based Truchas-PBF does not
yet have coupled heat transfer and flow.

The `data` directory contains a description of the experiment and data
collected by NIST, mostly from the 2018 AM Bench Challenge.

The `*inp` files are provided as input to Truchas. The prefixes `stvA`, `stvB`,
and `stvC` refer to cases A, B, and C from NIST (varying laser power and
speed). The suffixes `a` and `b` refer to without and with flow, respectively.
For the `b` cases, the integer suffixes `0`, `1`, and `2` refer to the
simulation stages: heat transfer only, then heat transfer and flow, then heat
transfer, flow, and probes. Probes are only enabled at the final stage due to
an associated memory leak currently in Truchas. The sbatch scripts `truchasA`,
`truchasB`, and `truchasC` queue array jobs on an HPC machine. They are
currently set to request 4 nodes on Snow.

The `results` folder contains results for 2 micron and 5 micron resolution runs.
This includes temperature probe data and a snapshot of volume fractions in the
vicinity of the melt pool, along with a Jupyter notebook for processing that
data.
