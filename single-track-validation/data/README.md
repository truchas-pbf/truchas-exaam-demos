Data from the AM Bench 2018 Challenge
-------------------------------------

* Single Track Test Description (AMB2018-02)
  https://www.nist.gov/ambench/amb2018-02-description

  * Melt Pool Length Data (CHAL-AMB2018-02-MP)
    https://www.nist.gov/ambench/chal-amb2018-02-mp-length

  * Melt Pool Width/Depth Data (CHAL-AMB2018-02-xsection)
    https://www.nist.gov/ambench/chal-amb2018-02-mp-xsection

  * Cooling Rate Data (CHAL-AMB2018-02-CR)
    https://www.nist.gov/ambench/chal-amb2018-02-cr

* The `AMB2018_AMMTLaserPower` directory contains laser power data for
  NIST's AM Metrology Testbed machine used in AM Bench 2018 Challenge.

### Other Data
* `Absorptivity update_ECP_20180505_Ye-Matthews.pdf`. LLNL experiments done
  for ECP/ExaAM. Includes absorptivity data for laser on bare IN625 plate.

### Additional Links
* AM Bench main page: https://www.nist.gov/ambench
* AM-Bench 2018 Test Descriptions: https://www.nist.gov/ambench/challenges-and-descriptions
* AM-Bench 2018 Test Data: https://www.nist.gov/ambench/benchmark-test-data
