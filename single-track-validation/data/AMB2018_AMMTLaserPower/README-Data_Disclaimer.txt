Data Disclaimer:

The National Institute of Standards and Technology (NIST) uses its best efforts 
to deliver a high-quality copy of the Database and to verify that the data contained 
therein have been selected on the basis of sound scientific judgment. However, 
NIST makes no warranties to that effect, and NIST shall not be liable for any damage 
that may result from errors or omissions in the Database.